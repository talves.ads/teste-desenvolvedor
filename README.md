# Teste para Desenvolvedor na Giver

## Requisitos

Desenvolver uma aplicação onde seja possível importar um arquivo de clientes no formato .csv (incluido nesse repositório `.data/customers.csv`). Os dados devem ser persistidos em um banco de dados (MySQL preferencialmente).

Após importado, deve ser possível consultar esses dados em uma lista paginada, que deve ser carregada de forma assíncrona em formato JSON.

Também deve ser possível verificar algumas características dos dados importados, que podem ser exibidos em numa tabela ou gráfico: 
- Quantidade de clientes com ou sem sobrenome
- Quantidade de clientes com email válido ou inválido
- Quantidade de clientes com ou sem gênero

## Dependências

- Docker: >= 19;

## Setup

Crie um arquivo `.env` na raiz do projeto, copiando o arquivo `.env.example`. Não precisa editar nada, apenas criar a cópia.

Posteriormente, execute os seguintes comandos do seu terminal:

- `docker-compose build`;
- `docker-compose up`;

Aguarde o docker realizar os processos até o fim.

## Aplicação

Acesse: [localhost](http://localhost).

## Teste

- Clique na opção "Importar arquivo", no menu, e cadastre seu csv;
- Após o término do processo, você será redirecionado para a lsita de clientes importados e ordenados alfabeticamente pelo nome;
- Na "home", ficarão os gráficos solicitados nos requisitos, para acessá-la, basta clicar em "Giver", no menu;

## Melhorias

Não deixe de acessar as issues deste repositório para saber o que pode ser melhorado neste processo.