const mix = require('laravel-mix');
const tailwindcss = require('tailwindcss');

mix.js('resources/static/js/app.js', 'public/static/js')
    .sass('resources/static/sass/app.scss', 'public/static/css')
    .options({
        processCssUrls: false,
        postCss: [tailwindcss('./tailwind.config.js')],
    });