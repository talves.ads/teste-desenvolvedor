<?php

namespace App\Exceptions\Csv;

use Exception;

class InvalidFileHeadersException extends Exception
{
    protected $message = 'Cabeçalho do arquivo é inválido.';
}
