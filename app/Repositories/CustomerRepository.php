<?php

namespace App\Repositories;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class CustomerRepository extends Repository
{
    public function getStatistics()
    {
        $query = [
            '(SELECT COUNT(*) FROM customers) as total',
            '(SELECT COUNT(*) FROM customers where last_name IS NOT NULL) as total_with_last_name',
            '(SELECT COUNT(*) FROM customers where email_validated = true) as total_with_email_validated',
            '(SELECT COUNT(*) FROM customers where gender IS NOT NULL) as total_with_gender',
        ];

        return Arr::first(DB::select('SELECT ' . implode(',', $query)));
    }
}
