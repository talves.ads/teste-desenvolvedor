<?php

namespace App\Models;

use App\Models\Traits\DataValidator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class Customer extends Model
{
    use DataValidator;

    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'email_validated',
        'gender',
        'ip_address',
        'company',
        'city',
        'title',
        'website',
    ];

    const GENDERS = [
        'female' => 'Feminino',
        'male'   => 'Masculino',
    ];

    /**
     * @return string|void
     */
    public function getGenderShowAttribute()
    {
        if ($this->gender) {
            return Arr::get(self::GENDERS, $this->gender);
        }
    }

    /**
     * @param string|null $value
     * @return void
     */
    public function setLastNameAttribute(string $value = null)
    {
        if ($value) {
            $this->attributes['last_name'] = $value;
        }
    }

    /**
     * @param string|null $value
     * @return void
     */
    public function setGenderAttribute(string $value = null)
    {
        $gender = Str::lower($value);

        if (Arr::exists(self::GENDERS, $gender)) {
            $this->attributes['gender'] = $gender;
        }
    }

    /**
     * @param string|null $email
     * @return void
     */
    public function setEmailAttribute(string $email = null)
    {
        $this->attributes['email'] = $email;
        $this->attributes['email_validated'] = $this->isValid($email, 'required|email');
    }
}
