<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Upload extends Model
{
    protected $table = 'uploads';

    protected $fillable = [
        'filename',
        'filename_original',
        'processed',
    ];

    const PATH = 'app/';

    public function getFilePathAttribute()
    {
        return storage_path(self::PATH . $this->filename);
    }

    /**
     * @return void
     */
    public function setProcessed()
    {
        $this->update(['processed' => true]);
    }
}
