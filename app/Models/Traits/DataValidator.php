<?php

namespace App\Models\Traits;

use Illuminate\Support\Facades\Validator;

trait DataValidator
{
    /**
     * @param mixed $value
     * @param string $rules
     * @return bool
     */
    public function isValid($value, string $rules)
    {
        $validator = Validator::make(compact('value'), ['value' => $rules]);

        return !$validator->fails();
    }
}