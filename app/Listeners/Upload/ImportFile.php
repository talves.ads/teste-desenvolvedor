<?php

namespace App\Listeners\Upload;

use App\Events\Upload\Created;
use App\Jobs\Customer\ImportFromCsv;

class ImportFile
{
    /**
     * Handle the event.
     *
     * @param Created $event
     * @return void
     */
    public function handle(Created $event)
    {
        ImportFromCsv::dispatchNow($event->upload);
    }
}
