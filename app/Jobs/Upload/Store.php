<?php

namespace App\Jobs\Upload;

use App\Events\Upload\Created;
use App\Models\Upload;
use App\Repositories\UploadRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\UploadedFile;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class Store implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var UploadedFile
     */
    private $file;

    /**
     * @var Upload
     */
    private $upload;

    /**
     * Create a new job instance.
     *
     * @param UploadedFile $file
     */
    public function __construct(UploadedFile $file)
    {
        $this->file = $file;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $upload = (new UploadRepository)->create($this->defineData());

        event(new Created($upload));
    }

    /**
     * @return array
     */
    private function defineData()
    {
        return [
            'filename'          => $this->file->store('/'),
            'filename_original' => $this->file->getClientOriginalName(),
        ];
    }
}
