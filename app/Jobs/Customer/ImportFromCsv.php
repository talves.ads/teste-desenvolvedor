<?php

namespace App\Jobs\Customer;

use App\Models\Upload;
use App\Repositories\CustomerRepository;
use App\Services\Csv\CustomerImporterService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ImportFromCsv implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var Upload
     */
    private $upload;

    /**
     * Create a new job instance.
     *
     * @param Upload $upload
     */
    public function __construct(Upload $upload)
    {
        $this->upload = $upload;
    }

    /**
     * Execute the job.
     *
     * @param CustomerRepository $repository
     * @return void
     * @throws \League\Csv\Exception
     */
    public function handle(CustomerRepository $repository)
    {
        $customers = (new CustomerImporterService($this->upload->file_path))->resolve();

        foreach ($customers as $customer) {
            $repository->create($customer);
        }

        $this->upload->setProcessed();
    }
}
