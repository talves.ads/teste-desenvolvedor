<?php


namespace App\Services\Csv;


class CustomerImporterService extends BaseImporterService
{
    protected $headers = [
        'first_name',
        'last_name',
        'email',
        'gender',
        'ip_address',
        'company',
        'city',
        'title',
        'website',
    ];
}