<?php

namespace App\Services\Csv;

use App\Exceptions\Csv\InvalidFileHeadersException;
use Illuminate\Support\Arr;
use League\Csv\Reader;
use League\Csv\Statement;

class BaseImporterService
{
    /**
     * @var string
     */
    private $filePath;

    /**
     * @var array
     */
    protected $headers;

    /**
     * @var \League\Csv\AbstractCsv|Reader
     */
    private $csvFile;

    /**
     * @var array
     */
    private $response = [];

    /**
     * BaseImporterService constructor.
     * @param string $filePath
     */
    public function __construct(string $filePath)
    {
        $this->filePath = $filePath;
    }

    /**
     * @return array
     * @throws \League\Csv\Exception
     */
    public function resolve()
    {
        $this
            ->loadFile()
            ->configHeaders()
            ->processFile();

        return $this->response;
    }

    /**
     * @return $this
     */
    private function loadFile()
    {
        $this->csvFile = Reader::createFromPath($this->filePath);

        return $this;
    }

    /**
     * @return $this
     * @throws \League\Csv\Exception
     * @throws \Exception
     */
    private function configHeaders()
    {
        if ($this->headers) {
            $this->csvFile->setHeaderOffset(0);

            $this->validateHeaders();
        }

        return $this;
    }

    /**
     * @return void
     * @throws \Exception
     */
    private function validateHeaders()
    {
        $intersect = array_intersect($this->csvFile->getHeader(), $this->headers);

        if (count($intersect) !== count($this->headers)) {
            throw new InvalidFileHeadersException;
        }
    }

    /**
     * @return void
     */
    private function processFile()
    {
        $rows = (new Statement)->process($this->csvFile);

        foreach ($rows as $row) {
            array_push($this->response, $this->defineRowData($row));
        }
    }

    /**
     * @param array $row
     * @return array
     */
    private function defineRowData(array $row)
    {
        if ($this->headers) {
            return Arr::only($row, $this->headers);
        }

        return $row;
    }
}