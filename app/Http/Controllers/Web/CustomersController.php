<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;

class CustomersController extends Controller
{
    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('customers.index');
    }
}
