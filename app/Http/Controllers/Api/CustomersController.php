<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\CustomerResource;
use App\Http\Resources\CustomersStatisticsResource;
use App\Repositories\CustomerRepository;

class CustomersController extends Controller
{
    /**
     * @var CustomerRepository
     */
    private $customerRepo;

    /**
     * CustomersController constructor.
     */
    public function __construct()
    {
        $this->customerRepo = new CustomerRepository;
    }

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return CustomerResource::collection(
            $this->customerRepo->paginate(20, 'first_name')
        );
    }

    /**
     * @return CustomersStatisticsResource
     */
    public function statistics()
    {
        return new CustomersStatisticsResource(
            $this->customerRepo->getStatistics()
        );
    }
}
