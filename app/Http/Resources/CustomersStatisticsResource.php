<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CustomersStatisticsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'total'                         => $this->total,
            'total_with_last_name'          => $this->total_with_last_name,
            'total_without_last_name'       => $this->total - $this->total_with_last_name,
            'total_with_email_validated'    => $this->total_with_email_validated,
            'total_without_email_validated' => $this->total - $this->total_with_email_validated,
            'total_with_gender'             => $this->total_with_gender,
            'total_without_gender'          => $this->total - $this->total_with_gender,
        ];
    }
}
