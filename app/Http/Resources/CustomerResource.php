<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CustomerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'first_name'  => $this->first_name,
            'last_name'   => $this->last_name,
            'email'       => $this->email,
            'gender_show' => $this->gender_show,
            'ip_address'  => $this->ip_address,
            'company'     => $this->company,
            'city'        => $this->city,
            'title'       => $this->title,
            'website'     => $this->website,
        ];
    }
}
