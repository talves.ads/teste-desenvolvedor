<?php

use App\Http\Controllers\Web\CustomersController;
use App\Http\Controllers\Web\DashboardController;
use App\Http\Controllers\Web\UploadsController;
use Illuminate\Support\Facades\Route;


Route::get('/', [DashboardController::class, 'index'])->name('home');

Route::resource('uploads', UploadsController::class, ['only' => ['create', 'store']]);

Route::get('customers', [CustomersController::class, 'index'])->name('customers');
