<?php

use App\Http\Controllers\Api\CustomersController;
use Illuminate\Support\Facades\Route;


Route::get('customers', [CustomersController::class, 'index']);
Route::get('customers/statistics', [CustomersController::class, 'statistics']);
