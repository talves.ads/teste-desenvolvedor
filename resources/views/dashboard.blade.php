@extends('_layouts.master')

@section('title', 'Clientes')

@section('content')
    <dashboard inline-template>
        <div class="w-full rounded overflow-hidden bg-white shadow-md mx-auto">
            <div class="px-6 py-4">
                <div class="font-bold text-xl mb-2 text-gray-800">Estatísticas dos clientes</div>
                <hr>
            </div>

            <div v-if="statistics.total === 0" class="block text-center py-8 text-gray-800">
                Ainda não há dados para serem processados.
            </div>

            <div class="px-6 py-4 md:flex items-center" v-else>
                <div class="w-full block md:w-1/4 md:inline-block mx-auto py-4">
                    <doughnut-chart :data="[statistics.total_with_last_name, statistics.total_without_last_name]"
                                    :labels="['Com gênero', 'Sem gênero']" :colors="['#805AD5', '#D53F8C']">
                    </doughnut-chart>
                </div>
                <div class="w-full block md:w-1/4 md:inline-block mx-auto py-4">
                    <doughnut-chart :labels="['E-mails válidos', 'E-mails inválidos']" :colors="['#3182CE', '#E53E3E']"
                                    :data="[statistics.total_with_email_validated, statistics.total_without_email_validated]">
                    </doughnut-chart>
                </div>
                <div class="w-full block md:w-1/4 md:inline-block mx-auto py-4">
                    <doughnut-chart :data="[statistics.total_with_gender, statistics.total_without_gender]"
                                    :labels="['Com gênero', 'Sem gênero']" :colors="['#38A169', '#D69E2E']">
                    </doughnut-chart>
                </div>
            </div>
        </div>
    </dashboard>
@endsection
