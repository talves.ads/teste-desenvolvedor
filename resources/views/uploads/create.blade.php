@extends('_layouts.master')

@section('title', 'Importar arquivo')

@section('content')

    <div class="w-full sm:w-1/2 rounded overflow-hidden bg-white shadow-md mx-auto">
        <div class="px-6 py-4">
            <div class="font-bold text-xl mb-2 text-gray-800">Importar arquivo</div>
            <hr>
        </div>

        <div class="px-6 py-4">
            <form action="{{ route('web.uploads.store') }}" method="post" enctype="multipart/form-data">
                @csrf

                <div class="mb-4">
                    <label class="default" for="file">
                        Arquivo:
                    </label>

                    <input type="file" name="file" id="file">

                    @include('_layouts.form-error', ['field' => 'file'])
                </div>

                <div class="flex pt-4 items-center justify-between">
                    <button class="default-submit" type="submit">Salvar</button>
                </div>
            </form>
        </div>
    </div>

@endsection
