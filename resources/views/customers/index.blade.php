@extends('_layouts.master')

@section('title', 'Clientes')

@section('content')
    <customers-index inline-template>
        <div class="w-full rounded overflow-hidden bg-white shadow-md mx-auto">
            <div class="px-6 py-4">
                <div class="font-bold text-xl mb-2 text-gray-800">Clientes</div>
                <hr>
            </div>

            <div class="px-6 py-4">
                <table class="table-auto border-collapse w-full">
                    <thead>
                    <tr class="rounded-lg font-medium text-gray-700 text-left">
                        <th class="px-4 py-2 bg-gray-200">Nome</th>
                        <th class="px-4 py-2 bg-gray-200">Sobrenome</th>
                        <th class="px-4 py-2 bg-gray-200">E-mail</th>
                        <th class="px-4 py-2 bg-gray-200">Gênero</th>
                        <th class="px-4 py-2 bg-gray-200">Empresa</th>
                        <th class="px-4 py-2 bg-gray-200">Profissão</th>
                        <th class="px-4 py-2 bg-gray-200">Cidade</th>
                        <th class="px-4 py-2 bg-gray-200">Site</th>
                    </tr>
                    </thead>
                    <tbody class="text-sm font-normal text-gray-700">
                    <tr v-for="customer in customers" class="hover:bg-gray-100 border-b border-gray-200 py-10">
                        <td class="px-4 py-4">@{{ customer.first_name }}</td>
                        <td class="px-4 py-4">@{{ customer.last_name }}</td>
                        <td class="px-4 py-4">@{{ customer.email }}</td>
                        <td class="px-4 py-4">@{{ customer.gender_show }}</td>
                        <td class="px-4 py-4">@{{ customer.company }}</td>
                        <td class="px-4 py-4">@{{ customer.title }}</td>
                        <td class="px-4 py-4">@{{ customer.city }}</td>
                        <td class="px-4 py-4">
                            <a v-if="customer.website" :href="customer.website" target="_blank" class="hover:underline">
                                Link
                            </a>
                        </td>
                    </tr>
                    <tr v-if="customers && customers.length === 0">
                        <td colspan="9" class="py-6 text-center">
                            Nenhum cliente cadastrado.
                        </td>
                    </tr>
                    </tbody>
                </table>

                <paginate v-if="lastPage > 1" :page-count="lastPage" :click-handler="loadCustomers" :prev-text="'<'"
                          :next-text="'>'" :container-class="'pagination my-4'">
                </paginate>
            </div>
        </div>
    </customers-index>
@endsection
