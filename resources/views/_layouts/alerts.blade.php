@if($success = session()->get('success'))
    <flash-alert text="{{ $success }}"></flash-alert>
@endif

@if($error = session()->get('error'))
    <flash-alert text="{{ $error }}" theme="error"></flash-alert>
@endif