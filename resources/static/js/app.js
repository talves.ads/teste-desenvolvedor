import Navbar from "./components/Navbar";
import Paginate from 'vuejs-paginate';
import FlashAlert from "./components/FlashAlert";
import Index from "./views/customers/Index";
import Dashboard from "./views/Dashboard";

window.Vue = require('vue');

// components
Vue.component('navbar', Navbar);
Vue.component('paginate', Paginate)
Vue.component('flash-alert', FlashAlert);

//views
Vue.component('dashboard', Dashboard);
Vue.component('customers-index', Index);

new Vue({
    el: '#app',
});
