import BaseMethods from "../BaseMethods";
import getData from "../utils/getData";

export default new class Audits extends BaseMethods {
    baseUrl = "/api/customers";

    async statistics() {
        const url = this.baseUrl + '/statistics';

        return this.api.get(url).then(getData);
    }
};
